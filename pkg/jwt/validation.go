package jwt

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var Auth *Authorisation

type (
	Token struct {
		jwt.StandardClaims
		ExpiresAt int64  `json:"exp,omitempty"`
		IssuedAt  int64  `json:"iat,omitempty"`
		Username  string `json:"Username,omitempty"`
		IsAdmin   bool   `json:"IsAdmin,omitempty"`
	}

	Authorisation struct {
		Secret string
	}
)

func Init(a *Authorisation) {
	Auth = a
}

func ValidateHeader(bearerHeader string) (interface{}, error) {
	tokenObject := &Token{}
	token, err := jwt.ParseWithClaims(bearerHeader, tokenObject, func(token *jwt.Token) (interface{}, error) {
		return []byte(Auth.Secret), nil
	})
	if err != nil {
		return tokenObject, err
	}
	if !token.Valid {
		return tokenObject, fmt.Errorf("Token is out of date ")
	}
	if tokenObject.ExpiresAt < time.Now().Unix() {
		return tokenObject, fmt.Errorf("Token is out of date ")
	}
	return tokenObject, nil
}

func GenerateJwt(username string, isAdmin bool, lifetime int64) (string, error) {
	tk := &Token{
		Username:  username,
		IsAdmin:   isAdmin,
		IssuedAt:  time.Now().Unix(),
		ExpiresAt: time.Now().Unix() + lifetime,
	}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	return token.SignedString([]byte(Auth.Secret))
}
