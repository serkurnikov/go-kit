package apierrors

import (
	"fmt"
	"net/http"
)

type APIError struct {
	HTTPCode    int    `json:"-"`
	Code        int    `json:"code,omitempty"`
	Message     string `json:"message,omitempty"`
	Description string `json:"description,omitempty"`
}

func (ae APIError) WithDescription(description string) APIError {
	ae.Description = description
	return ae
}

func (ae APIError) Error() string {
	return fmt.Sprintf("error code %d: %s: %s", ae.Code, ae.Message, ae.Description)
}

//noinspection GoUnusedGlobalVariable
var (
	Internal = APIError{
		HTTPCode: http.StatusInternalServerError,
		Code:     500,
		Message:  "Internal Server Error",
	}
	Timeout = APIError{
		HTTPCode: http.StatusGatewayTimeout,
		Code:     504,
		Message:  "Gateway Timeout",
	}
	NotFound = APIError{
		HTTPCode: http.StatusNotFound,
		Code:     404,
		Message:  "Not Found",
	}
	ServiceUnavailable = APIError{
		HTTPCode: http.StatusServiceUnavailable,
		Code:     503,
		Message:  "Service Unavailable",
	}
	RequestTimeout = APIError{
		HTTPCode: http.StatusRequestTimeout,
		Code:     504,
		Message:  "Request Timeout",
	}
	TooManyRequests = APIError{
		HTTPCode: http.StatusTooManyRequests,
		Code:     429,
		Message:  "Too many requests",
	}
)

//noinspection GoUnusedGlobalVariable
var (
	ResponseFormatterError = Internal.WithDescription("response formatter error")

	AuthBadFormat = APIError{
		HTTPCode:    http.StatusUnauthorized,
		Code:        1004,
		Message:     "Unsupported authorization method",
		Description: "",
	}
	AuthForbidden = APIError{
		HTTPCode:    http.StatusForbidden,
		Code:        1003,
		Message:     "Action is forbidden for this API key",
		Description: "",
	}
	AuthUnauthorized = APIError{
		HTTPCode:    http.StatusUnauthorized,
		Code:        1002,
		Message:     "Authorization is required or has been failed",
		Description: "",
	}

	Validation = APIError{
		HTTPCode:    http.StatusBadRequest,
		Code:        10001,
		Message:     "Validation error",
		Description: "",
	}
	IncorrectAddress = Validation.WithDescription("incorrect address")
	IncorrectID      = Validation.WithDescription("incorrect ID")
	InvalidRequest   = Validation.WithDescription("cannot parse request")

	DuplicateRequest = APIError{
		HTTPCode: http.StatusBadRequest,
		Code:     20043,
		Message:  "Duplicate request",
	}

	NotAllowed = APIError{
		HTTPCode: http.StatusBadRequest,
		Code:     600,
		Message:  "Action not allowed",
	}
)
