package helper

import (
	"strings"

	"golang.org/x/crypto/bcrypt"
)

func DeleteDuplicate(strSlice []string) []string {
	allKeys := make(map[string]bool)
	var list []string
	for _, item := range strSlice {
		if _, ok := allKeys[item]; !ok {
			allKeys[item] = true
			if len(item) > 0 {
				list = append(list, item)
			}
		}
	}
	return list
}

func Contains(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[strings.ToLower(s)] = struct{}{}
	}

	_, ok := set[strings.ToLower(item)]
	return ok
}

func HashPassword(password string) (string, error) {
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(passwordHash), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
