package def

import (
	"net/http"

	"github.com/powerman/must"
	"github.com/prometheus/client_golang/prometheus"
)

// Init must be called once before using this package.
// It provides common initialization for both commands and tests.
func Init() {
	// Make sure no one occasionally uses global objects.
	http.DefaultServeMux = nil
	prometheus.DefaultRegisterer = nil
	prometheus.DefaultGatherer = nil

	must.AbortIf = must.PanicIf

	setupLog()
}
