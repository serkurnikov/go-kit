package mongo

import (
	"context"
	"errors"
	"runtime/debug"
	"time"

	"github.com/powerman/structlog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	migrationCollection = "migrations"
)

type MigrationScript func(ctx context.Context, db *mongo.Database, logger *structlog.Logger) error
type Migration struct {
	Name   string
	Script MigrationScript
}

type MigrationModel struct {
	Name string `json:"name" bson:"name"`
	TS   int64  `json:"ts" bson:"ts"`
}

func createMigration(ctx context.Context, db *mongo.Database, m *MigrationModel) error {
	_, e := db.Collection(migrationCollection).InsertOne(ctx, m)
	return e
}

func getMigration(ctx context.Context, db *mongo.Database, name string) (*MigrationModel, error) {
	filter := bson.M{
		"name": name,
	}
	m := &MigrationModel{}

	err := db.Collection(migrationCollection).FindOne(ctx, filter).Decode(m)
	if err != nil {
		return nil, err
	}
	if m.Name != name {
		return nil, errors.New("migration not found")
	}

	return m, nil
}

func migrateData(ctx context.Context, db *mongo.Database, name string, f MigrationScript, logger *structlog.Logger) error {
	defer func() {
		r := recover()
		if r != nil {
			logger.Fatalf("Migration error: %s '\n' %s", r, string(debug.Stack()))
		}
	}()

	m, err := getMigration(ctx, db, name)
	if err != nil {
		return err
	}
	if m == nil {
		logger.Info("Migrate:", name)
		if err := f(ctx, db, logger); err != nil {
			return err
		}

		m := &MigrationModel{Name: name, TS: time.Now().Unix()}
		if err := createMigration(ctx, db, m); err != nil {
			return err
		}
	}
	return nil
}

func ApplyMigrations(ctx context.Context, db *mongo.Database, migrations []Migration, logger *structlog.Logger) error {
	for _, m := range migrations {
		if err := migrateData(ctx, db, m.Name, m.Script, logger); err != nil {
			return err
		}
	}
	return nil
}
