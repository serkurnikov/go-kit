package mongo

import "fmt"

type Config struct {
	URL      string
	Database string
}

func (c *Config) FormatDSN() string {
	return fmt.Sprintf(
		"url=%s database=%s", c.URL, c.Database,
	)
}
