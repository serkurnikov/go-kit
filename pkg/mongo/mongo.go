package mongo

import (
	"context"

	"github.com/powerman/structlog"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/serkurnikov/go-kit/pkg/cache/memory"
)

func Init(ctx context.Context, log *structlog.Logger, cfg *Config, c *memory.Storage, setup ...func(ctx context.Context, log *structlog.Logger, db *mongo.Database, c *memory.Storage) error) (*mongo.Database, *mongo.Client, error) {
	log.Println("Starting Mongo:", cfg.URL)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.URL))
	if err != nil {
		log.Fatalf("failed connect mongo: %s", err.Error())
		return nil, nil, err
	}
	db := client.Database(cfg.Database)

	for _, function := range setup {
		if err = function(ctx, log, db, c); err != nil {
			log.Fatalf("failed setup function: %s", err.Error())
			return nil, nil, err
		}
	}

	return db, client, nil
}

func Close(ctx context.Context, c *mongo.Client, log *structlog.Logger) {
	err := c.Disconnect(ctx)
	if err != nil {
		log.Fatalf("failed close mongo: %s", err.Error())
	}
}
