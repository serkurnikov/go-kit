package mongo

import (
	"context"

	"github.com/powerman/structlog"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/serkurnikov/go-kit/pkg/cache/memory"
	"gitlab.com/serkurnikov/go-kit/pkg/reflectx"
)

type Repo struct {
	Database *mongo.Database
	Client   *mongo.Client
	Logger   *structlog.Logger
	Cache    *memory.Storage
	Metric   Metrics
}

func New(cfg *Config, log *structlog.Logger, setup ...func(ctx context.Context, logger *structlog.Logger, db *mongo.Database, c *memory.Storage) error) (*Repo, error) {
	storage := memory.InitCache(memory.CachedTime, memory.ClearedTime)
	db, client, err := Init(context.Background(), log, cfg, storage, setup...)
	if err != nil {
		log.Println("Mongo Init error:", err.Error())
		return nil, err
	}

	return &Repo{
		Database: db,
		Client:   client,
		Logger:   log,
		Cache:    storage,
	}, nil
}

func (r *Repo) Disconnect() {
	Close(context.Background(), r.Client, r.Logger)
}

// NoTx provides DAL method wrapper with:
// - converting mongo errors which are actually bugs into panics,
// - general metrics for DAL methods,
// - wrapping errors with DAL method name.
func (r *Repo) NoTx(f func() error) (err error) {
	methodName := reflectx.CallerMethodName(1)
	return r.Metric.instrument(methodName, func() error {
		err := f()
		return err
	})()
}
