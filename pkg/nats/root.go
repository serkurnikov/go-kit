package nts

import (
	"encoding/json"

	"github.com/nats-io/nats.go"
)

var nc *nats.Conn

func InitNats(url string) error {
	var err error
	nc, err = nats.Connect(url)
	return err
}

func GetNatsConn() *nats.Conn {
	return nc
}

func Close() {
	nc.Close()
}

func PublishToNats(subj string, data interface{}) error {
	bData, err := json.Marshal(data)
	if err != nil {
		return err
	}
	return nc.Publish(subj, bData)
}
