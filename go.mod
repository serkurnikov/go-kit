module gitlab.com/serkurnikov/go-kit

go 1.18

require (
	github.com/cenkalti/backoff/v4 v4.1.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.6
	github.com/nats-io/nats.go v1.16.0
	github.com/powerman/getenv v0.1.0
	github.com/powerman/go-service-example v1.4.0
	github.com/powerman/must v0.1.1
	github.com/powerman/structlog v0.7.3
	github.com/pressly/goose v2.7.0+incompatible
	github.com/prometheus/client_golang v1.13.0
	go.mongodb.org/mongo-driver v1.10.1
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/nats-io/nkeys v0.3.0 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.37.0 // indirect
	github.com/prometheus/procfs v0.8.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
